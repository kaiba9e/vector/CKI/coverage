#!/usr/bin/python3
""" Create a map from task name to uid """

import sys
import os
import json
import collections

#
# Note we use oyaml which is a drop-in replacement for PyYAML which preserves
# dict ordering. And you have to install PyYAML first, then have a try, e.g.
# ```bash
#    $ git clone https://github.com/wimglenn/oyaml.git /tmp/oyaml
#    $ export PYTHONPATH=/tmp/oyaml:$PYTHONPATH
# ```
#
try:
    import oyaml as yaml
except ModuleNotFoundError:
    import yaml


KPET_DB = None

def load_obj_byyamlfile(yaml_file):
    txt = None
    with open(yaml_file, 'r') as file_handle:
        txt = ''.join(file_handle.readlines())
    obj = yaml.safe_load(txt)
    return obj


def simplify(case):
    out = {}
    out['_id'] = case['_id']

    if 'name' not in case.keys():
        return None
    out['name'] = case['name']

    if 'universal_id' not in case.keys():
        return None
    out['uid'] = case['universal_id']

    if 'waived' not in case.keys():
        out['waived'] = False
    else:
        out['waived'] = True

    return out


def main(argc, argv):
    if argc != 2:
        print("Usage: %s <kpet-db>" % argv[0], file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s /tmp/kpet-db", file=sys.stderr)
        return 1

    global KPET_DB
    KPET_DB = argv[1]
    kpet_db_index = '%s/index.yaml' % KPET_DB

    obj_index = load_obj_byyamlfile(kpet_db_index)
    if obj_index is None:
        return 1

    d_cases = obj_index['case']['cases']
    l_out = []
    for key in d_cases.keys():
        case_index = '%s/%s' % (KPET_DB, d_cases[key])
        #print('>>> Init %s ...' % case_index, file=sys.stderr)
        case_obj = load_obj_byyamlfile(case_index)
        if case_obj is None:
            continue
        case_obj['_id'] = key # XXX: case name saved in index.yaml
        case = simplify(case_obj)
        if case is None:
            continue
        l_out.append(case)

    out = yaml.dump(l_out, indent=2)
    print(out)

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))

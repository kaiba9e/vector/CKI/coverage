#!/bin/bash
#
# Get CKI test (i.e. task) list
#

FILE=$(readlink -f $BASH_SOURCE)
NAME=$(basename $FILE)
CDIR=$(dirname $FILE)
TMPDIR=${TMPDIR:-"/tmp"}
UTIL_RKW=$CDIR/run-kpet-wrapper

function cleanup
{
	rm -rf $TMPDIR/$NAME.*.$$
}

function get_arch_list_bytree
{
	typeset tree=${1:-"rhel8"}
	typeset arches=$($UTIL_RKW arch list --tree $tree | sort -r)
	echo $arches
}

function run
{

	typeset tree=${1:-"rhel8"}
	typeset arches=$(get_arch_list_bytree $tree)
	typeset arch=""
	for arch in $arches; do
		echo "# Test (task) list of $tree/$arch"
		echo "- tree: $tree"
		echo "  arch: $arch"
		echo "  tasks:"
		$UTIL_RKW run test list --tree $tree --arch $arch | \
			sed -e 's/^/    - "/g' -e 's/$/"/g'
		echo "  misc:"
		echo "    desc: CKI test list for $tree/$arch"
		echo "    created_by: $(id -un)"
		echo "    created_date: $(date +\"%Y/%m/%d\")"
		echo
	done
}


tree=${1:-"rhel8"}
trap "cleanup" EXIT
run $tree
exit 0

#!/bin/bash
#
# A wrapper to run kpet with kpet-db + cki-lib
#
# """ CentOS 7: sudo yum -y install python36-jinja2 """
# """           sudo yum -y install python36-lxml   """
# """           sudo yum -y install python36-ply    """
#

FILE=$(readlink -f $BASH_SOURCE)
NAME=$(basename $FILE)
CDIR=$(dirname $FILE)
TMPDIR=${TMPDIR:-"/tmp"}

tree=${1:-"rhel8"}
arch=${2:-"x86_64"}

typeset -a a_repo
a_repo[0]="cki-lib https://gitlab.com/cki-project/cki-lib.git"
a_repo[1]="kpet    https://gitlab.com/cki-project/kpet.git"
a_repo[2]="kpet-db https://gitlab.com/cki-project/kpet-db.git"

function setup
{
	typeset -i rc=0
	typeset -i i=0
	for (( i = 0; i < ${#a_repo[@]}; i++ )); do
		repo_name=$(echo ${a_repo[$i]} | awk '{print $1}')
		repo_url=$(echo  ${a_repo[$i]} | awk '{print $2}')
		[[ -d $TMPDIR/$repo_name ]] && continue

		echo "git clone $repo_url $TMPDIR/$repo_name ..."
		rm -rf "$TMPDIR/$repo_name"
		git clone "$repo_url" "$TMPDIR/$repo_name"
		(( rc += $? ))
	done

	return $rc
}

function cleanup
{
	rm -rf $TMPDIR/$NAME.*.$$
}

setup || exit 1
trap "cleanup" EXIT

# XXX: must extend the PYTHONPATH
export PYTHONPATH=$TMPDIR/cki-lib:$TMPDIR/kpet:$PYTHONPATH

pushd "$(pwd)" > /dev/null 2>&1
cd $TMPDIR/kpet
python3 -m kpet --db $TMPDIR/kpet-db "$@"
ret=$?
popd > /dev/null 2>&1
exit $ret

# HOWTO

## 01. Install packages used by kpet

### CentOS 7
```
sudo yum -y install python36-jinja2
sudo yum -y install python36-lxml
sudo yum -y install python36-ply
```

## 02. Get test(i.e. task) list for rhel8

```bash
#
# Step 1 - get the source code
#
git clone https://gitlab.com/kaiba9e/vector/CKI/coverage.git

#
# Step 2 - create tasks.yaml file
#
cd coverage/utils
make
./get-test-list rhel8 > /tmp/tasks.yaml

#
# Step 3 - convert tasks.yaml to tasks.json
#
git clone https://gitlab.com/kaiba9e/vector/CKI/yamlutils.git
cd yamlutils
make
./yamladm /tmp/tasks.yaml > /tmp/tasks.json
```

## 03. Get test(i.e. task) map (between name and universal_id)

Note the name often has such format:
* `basename` - `sub case`, e.g.
  * KVM Self Tests - AMD
  * KVM Self Tests - Intel

The `basename` is `KVM Self Tests`, sub cases are `AMD` and `Intel`

```
cd coverage/utils
make
./kpetdb-test2uid /tmp/kpet-db > /tmp/tasksmap.yaml
yamladm /tmp/tasksmap.yaml > /tmp/tasksmap.json
```

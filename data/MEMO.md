# Baseline commit of utilities to get test list

* Date: 2022/3/11
* Utils:
  * cki-lib: `49e1ca94e7a7c6a44f58526c2916e7ddf9b9e025`
  * kpet-db: `e66532b6781236747a234ca63e177c6a97e00e2b`
  * kpet   : `5c25b74ab50513ce17412260d53feec8db849f48`


## 01. cki-lib

* https://gitlab.com/cki-project/cki-lib.git

```
commit 49e1ca94e7a7c6a44f58526c2916e7ddf9b9e025
Merge: 6b45727 c5e8a9d
Author: Iñaki Malerba <inaki@malerba.space>
Date:   Wed Mar 9 15:16:21 2022 +0000

    Merge: cki_entrypoint: Allow custom flask app path

    Not all the applications use 'app' as the flask app. Allow customizing
    it.

    See merge request cki-project/cki-lib!372
```


## 02. kpet-db

* https://gitlab.com/cki-project/kpet-db.git

```
commit e66532b6781236747a234ca63e177c6a97e00e2b
Author: Bruno Goncalves <bgoncalv@redhat.com>
Date:   Thu Mar 10 15:55:06 2022 +0100

    rhel86-z and rhel90-z use same compose as their ystream

```


## 03. kpet

* https://gitlab.com/cki-project/kpet.git

```
commit 5c25b74ab50513ce17412260d53feec8db849f48
Author: Nikolai Kondrashov <Nikolai.Kondrashov@redhat.com>
Date:   Tue Dec 14 15:10:48 2021 +0200

    Add -f/--source-file option support

    Add support for -f/--source-file option specifying a changed source
    files to match tests against. Repeat to add more files. Merged with
    sources extracted from patch mailboxes.

    Fixes #45
```
